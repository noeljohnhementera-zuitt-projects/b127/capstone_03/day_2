import './App.css';
import { useState, useEffect } from 'react';

import Navbar from './components/Navbar';
// From the Home page
import Home from './pages/Home';

// From the Login
import Login from './pages/Login';

// From the Register.js
import Register from './pages/Register';

// Import Routers
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

//bootstrap
import { Container } from 'react-bootstrap';

// Import react context
import GlobalDataContext from './GlobalDataContext';

export default function App() {

  // useState() for login purposes
  const [ user, setUser ] = useState({
      customerAccessToken: localStorage.getItem('customerAccessToken'),
      email: localStorage.getItem('email'),
      address: localStorage.getItem('address'),
      paymentOption: localStorage.getItem('paymentOption'),
      isAdmin: localStorage.getItem('isAdmin') === 'true' // to avoid changing status after hard refresh
  })

  useEffect(() =>{
    console.log(user);
    console.log(localStorage)
  }, [user])

  return (
    <GlobalDataContext.Provider value= { {user, setUser} }>
        <Router>
        	< Navbar />
                <Switch>
                    < Route exact path = '/register' component={Register} />
                		< Route exact path = '/login' component={Login} />
            	    	< Route exact path = '/' component={Home} />
                </Switch>
        </Router>
    </GlobalDataContext.Provider>
   );
}