// Import bootstrap
import { Form, Button, Row, Col } from 'react-bootstrap';

// import from react
// import useContext() to get the data from App.js
import { Fragment, useState, useEffect, useContext } from 'react';

// import react Context
import GlobalDataContext from '../GlobalDataContext';

// import Swalfire
import Swal from 'sweetalert2';

import { Redirect, useHistory } from 'react-router-dom';

export default function Login () {

	const history = useHistory();

	// useContext() to unwrap the data that is passed sa App.js
	const { user, setUser } = useContext(GlobalDataContext);

	// useState() Input Fields
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	// useState() Login Button
	const [ isAuthenticated, setIsAuthenticated ] = useState(false);

	// useEffect() to enable / disable Login Button
	useEffect(() =>{
		if(email !== '' && password !== ''){
			setIsAuthenticated(true)
		}else{
			setIsAuthenticated(false)
		}
	}, [email, password])

	// Retrieve the data from our back-end
	const getInputData = (e) =>{
		e.preventDefault();	// prevents the default behavior of refreshing the page

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data) // check the accessToken
			if(data.customerAccessToken !== null){
				localStorage.setItem("customerAccessToken", data.customerAccessToken)
				setUser({customerAccessToken: data.customerAccessToken});

				Swal.fire({
					title: 'Successfully Logged In!',
					icon: 'success',
					text: 'Thank you for logging in to All Resto E-Commerce'
				})

				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization: `Bearer ${data.customerAccessToken}`
					}
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)

					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})

						history.push('/products')
					}else{
						history.push('/')
					}
				})
			}else{

				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Something Went Wrong. Check Your Credentials!'
				})
			}

			setEmail('')
			setPassword('')

		})
	}

	return (
	<Fragment>
		<h1 className='text-center padding-top-5'>Login</h1>
		<Row className='justify-content-center'>
			<Col md={{ span: 6 }}>
				<Form onSubmit={(e) => getInputData(e)}>
					<Form.Group>
						<Form.Label>Email Address:</Form.Label>
						<Form.Control
							type='text'
							value={email}
							// we can't enter anything bec of the useState hook email getter is ('')
							onChange={e => setEmail(e.target.value)}
							// use onChange to trigger the setter setEmail and become getter's (email) new value				
							placeholder='Enter Your Email Address'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type='text'
							value={password}
							onChange={e => setPassword(e.target.value)}
							placeholder='Enter Your Password'
							required
						/>
					</Form.Group>
					{isAuthenticated ?
					<Button variant='primary' type='submit'>Login</Button>
					:
					<Button variant='secondary' type='submit' disable>Login</Button>
					}
					{/*This will depend sa useEffect() statement*/}
				</Form>
			</Col>
		</Row>
	</Fragment>
	)
} 