import { Fragment, useState, useEffect } from 'react';

import { Form, Button, Row, Col } from 'react-bootstrap';

export default function Register () {

	// useState() for Button
	const [ isActive, setIsActive ] = useState(false);

	// useState() for inputs
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ address, setAddress ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ paymentOption, setPaymentOption ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	useEffect(() =>{
		if((firstName !== '' && lastName !== '' && address !== '' && email !== '' && paymentOption !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [firstName, lastName, address, email, paymentOption, password, verifyPassword])

	return (
	<Fragment>		
		<h1 className='text-center padding-top-5'>Register</h1>
		<Row className='justify-content-center'>
			<Col md={{ span: 6 }}>
				<Form>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control 
							type='text'
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
							placeholder='Enter Your First Name'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control 
							type='text'
							value={lastName}
							onChange={e => setLastName(e.target.value)}
							placeholder='Enter Your Last Name'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Address:</Form.Label>
						<Form.Control 
							type='text'
							value={address}
							onChange={e => setAddress(e.target.value)}
							placeholder='Enter Your Address'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
							type='email'
							value={email}
							onChange={e =>setEmail(e.target.value)}
							placeholder='Enter Your Email'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Payment Option:</Form.Label>
						<Form.Control 
							type='text'
							value={paymentOption}
							onChange={e => setPaymentOption(e.target.value)}
							placeholder='Enter Your Payment Option'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
							type='password'
							value={password}
							onChange={e => setPassword(e.target.value)}
							placeholder='Enter Your Password'
							required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Verify Password:</Form.Label>
						<Form.Control 
							type='password'
							value={verifyPassword}
							onChange={e => setVerifyPassword(e.target.value)}
							placeholder='Verify Your Password'
							required
						/>
					</Form.Group>
					{isActive ?
					<Button variant='primary' type='submit'>Register</Button>
					:
					<Button variant='secondary' type='submit' disable>Register</Button>
					}
				</Form>
			</Col>
		</Row>
	</Fragment>
	)
}