// First Step Imports
import { Navbar, Nav } from 'react-bootstrap';

// Import routers
import { Link, NavLink } from 'react-router-dom';


export default function AppNavbar () {
	return (
		<Navbar bg='primary' variant='dark' expand='lg'>
			<Navbar.Brand as={Link} to='/'>All-Resto-E-Commerce</Navbar.Brand>
			<Navbar.Toggle aria-controls='basic-navbar-nav'/>
			<Navbar.Collapse id='basic-navbar-nav'>
				<Nav className='ml-auto'>
					<Nav.Link as={NavLink} to='/'>Home</Nav.Link>
					<Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
					<Nav.Link as={NavLink} to='/reviews'>Reviews</Nav.Link>
					<Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
					<Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}