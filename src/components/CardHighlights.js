// First Step Imports
import { Row, Button, Col, Card } from 'react-bootstrap';

import { Fragment } from 'react';
export default function () {
	return (
		<Fragment>
			<h2 className='text-center padding-top-5'>Why Our Customer Love Us?</h2>
			<Row>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Body>
							<Card.Title><h2>One Stop Shop!</h2></Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Card.Text>
						</Card.Body>
					</Card>				
				</Col>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Body>
							<Card.Title><h2>Hassle-Free!</h2></Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Card.Text>
						</Card.Body>
					</Card>				
				</Col>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Body>
							<Card.Title><h2>Trusted by Many!</h2></Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Card.Text>
						</Card.Body>
					</Card>				
				</Col>
			</Row>
		</Fragment>
	)
}